//import './App.css';
import axios from 'axios';

import { withAuthenticator } from '@aws-amplify/ui-react'
import Amplify, { Auth } from "aws-amplify";
import awsExports from "./aws-exports";
import React from 'react';
Amplify.configure(awsExports);

var enableAuth = false; //to enable / disable auth on the web app
var apiID = '';// //your app id for your APIG Gateway/Lambda
var region = '';//AWS region where the API Gateway/Lambda is deployed to

class App extends React.Component{

  constructor(props) {
    super(props);
    this.state = { statusMessage : 'Click on the above buttons to trigger a call to API Gateway' }
  }

  callAPI = (apiID, sendBearerToken) => {
    Auth.currentSession().then(res=>{
      let idToken = res.getIdToken().getJwtToken();
      axios.get(
        `https://${apiID}.execute-api.${region}.amazonaws.com/Prod/hello`,
        {
          headers:{
            'Authorization': sendBearerToken ? `Bearer ${idToken}` : ``
          }
        }
      )
      .then((res) => {
        console.log(res);
        this.setState({
          statusMessage: res.data.message
        });
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          statusMessage: err.stack
        });
      })
    })
    .catch(err => {
      this.setState({
        statusMessage: err
      })
    })
  }

  render() {
    return (
      <div style={{ display: 'flex', justifyContent: 'center', alignItems:'center', height:'100vh' }}>
        <table>
          <tbody>
            <tr>
              <td style={{  display: 'flex', justifyContent: 'center', alignItems:'center' }}>
                <font size='20' >{`{ Hello World }`}</font>
                <br/> <br/> <br/> <br/>
              </td>
            </tr>
            <tr>
                <tr style={{  display: 'flex', justifyContent: 'center', alignItems:'center' }}>
                  <td style={{ padding:5 }}><button type='submit' style={{ padding:10 }} onClick={()=>{this.callAPI(apiID,false)}}>Call API w/o ID Token</button></td>
                  <td style={{ padding:5 }}><button type='submit' style={{ padding:10 }} onClick={()=>{this.callAPI(apiID,true)}}>Call API with ID Token</button></td>
                </tr>
            </tr>
            <tr >
              <td style={{ display: 'flex', justifyContent: 'center', alignItems:'center' }}>
                <br/> <br/> 
                <textarea style={{ width:'50vw', height:'20vh', border:0, textAlign: 'center'}} readOnly value={this.state.statusMessage}></textarea>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}
export default enableAuth ? withAuthenticator(App) : App;

